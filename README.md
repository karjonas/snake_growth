Snake Growth
============

A Ludum Dare 34 entry written by Jonas Karlsson.

Building
--------

Download sources:
```sh
$ wget http://cdn.cocos2d-x.org/cocos2d-x-3.9.zip
$ unzip cocos2d-x-3.9.zip -d cocos2d
$ mkdir bin
$ cd bin
$ cmake ..
$ make
```

License
-------

All graphics is licensed under the CC BY 4.0 License. See https://creativecommons.org/licenses/by/4.0/

The font Press Start 2P by CodeMan38 is licensed under the SIL Open Font License, 1.1.

All source code is licensed under the MIT License. See http://opensource.org/licenses/MIT

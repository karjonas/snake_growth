#ifndef GLOBALDATA_H
#define GLOBALDATA_H

#include <vector>
#include <string>

enum Dir { NORTH, EAST, SOUTH, WEST };

struct LevelData
{
  std::string filename;
  int segments;
  int speed;
  Dir dir;
  int health_value;
};

struct GlobalData
{
  GlobalData();

  int current_level_idx = 0;
  std::vector<LevelData> levels;
};

#endif // GLOBALDATA_H

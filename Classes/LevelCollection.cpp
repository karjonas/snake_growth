#include "LevelCollection.h"
#include "2d/CCTMXTiledMap.h"

#include <2d/CCSprite.h>

#include <2d/CCTMXLayer.h>

#include <iostream>

using namespace cocos2d;

LevelCollection::LevelCollection()
{
  pixelToBlockMap[0]= BlockType::EMPTY;
  pixelToBlockMap[1]= BlockType::UNIT;
  pixelToBlockMap[2]= BlockType::MINUS;
  pixelToBlockMap[3]= BlockType::HEALTH;
  pixelToBlockMap[4]= BlockType::EXIT;
  pixelToBlockMap[5]= BlockType::DEATH;
  pixelToBlockMap[6]= BlockType::WALL;
}

void LevelCollection::readPixmapBoard(TMXTiledMap* tiled_map)
{
  TMXLayer* layer = tiled_map->getLayer("layer0");

  std::vector<std::vector<BlockType>> positions{static_cast<size_t>(num_width), {static_cast<size_t>(num_height),BlockType::EMPTY}};

  for (int h = 0; h < num_height; h++)
  {
    for (int w = 0; w < num_width; w++)
    {
      uint32_t GID = layer->getTileGIDAt(Vec2(w, h));
      BlockType blockType = pixelToBlockMap.at(GID);
      positions[w][num_height - h - 1] = blockType;
    }
  }

  this->positions = positions;
}

void LevelCollection::setLevel(const std::string level_path)
{
  auto* tiled_map = TMXTiledMap::create(level_path);

  readPixmapBoard(tiled_map);

  tiled_map->retain();
  tiled_map->release();
}

#ifndef  BLOCKTYPE_H
#define  BLOCKTYPE_H

enum class BlockType { EMPTY, WALL, UNIT, EXIT, HEALTH, MINUS, DEATH, TAIL };

#endif //BLOCKTYPE_H

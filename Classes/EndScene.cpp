#include "EndScene.h"

USING_NS_CC;

Scene* EndScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = EndScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool EndScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

    this->scheduleUpdate();

    // creating a keyboard event listener
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(EndScene::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(EndScene::onKeyReleased, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    const Size visibleSize = Director::getInstance()->getVisibleSize();
    const float mid_h = visibleSize.height/2;
    const float mid_w = visibleSize.width/2;

    {
      auto level_label =  Label::createWithTTF("Congratulations!", "fonts/PressStart2P.ttf", 8);
      level_label->setPosition(mid_w,mid_h);
      addChild(level_label, 1);
    }

    return true;
}

// Implementation of the keyboard event callback function prototype
void EndScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
  if (std::find(heldKeys.begin(), heldKeys.end(), keyCode) == heldKeys.end())
    heldKeys.push_back(keyCode);
}

void EndScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
  heldKeys.erase(std::remove(heldKeys.begin(), heldKeys.end(), keyCode), heldKeys.end());
  releasedKeys.push_back(keyCode);
}

template<typename C, typename T>
bool contains(const C& c, const T& t)
{
  return std::find(c.begin(), c.end(), t) != c.end();
}

void EndScene::update(float dt)
{
  if (contains(releasedKeys, EventKeyboard::KeyCode::KEY_ESCAPE))
  {
      Director::getInstance()->end();
      return;
  }

  heldKeysLast = heldKeys;
  releasedKeys.clear();
  pressedKeys.clear();
}


void EndScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

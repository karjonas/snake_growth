#include "GlobalData.h"


GlobalData::GlobalData()
{



  {
    LevelData ld;
    ld.filename = "lev0.tmx";
    ld.segments = 68;
    ld.speed = 8;
    ld.health_value = 100;
    ld.dir = Dir::NORTH;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev2.tmx";
    ld.segments = 115;
    ld.speed = 5;
    ld.health_value = 100;
    ld.dir = Dir::NORTH;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev1.tmx";
    ld.segments = 55;
    ld.speed = 5;
    ld.health_value = 100;
    ld.dir = Dir::NORTH;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev3.tmx";
    ld.segments = 55;
    ld.speed = 3;
    ld.health_value = 100;
    ld.dir = Dir::NORTH;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev4.tmx";
    ld.segments = 45;
    ld.speed = 6;
    ld.health_value = 100;
    ld.dir = Dir::WEST;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev5.tmx";
    ld.segments = 25;
    ld.speed = 3;
    ld.dir = Dir::SOUTH;
    ld.health_value = 70;
    levels.push_back(ld);
  }

  {
    LevelData ld;
    ld.filename = "lev6.tmx";
    ld.segments = 205;
    ld.speed = 5;
    ld.dir = Dir::NORTH;
    ld.health_value = 70;
    levels.push_back(ld);
  }

}

#include "HelloWorldScene.h"
#include "EndScene.h"
#include "GlobalData.h"

USING_NS_CC;


float dir_to_rotation(const Dir dir)
{
  float rot = 0.0f;
  switch (dir)
  {
    case NORTH:
      rot = 0.0f;
      break;
    case SOUTH:
      rot = 180.0f;
      break;
    case EAST:
      rot = 90.0f;
      break;
    case WEST:
      rot = 270.0f;
      break;
  }
  return rot;
}

//////////////////////////////////////////////////////////////////////////////


Scene* HelloWorld::createScene(GlobalData global_data)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
    layer->post_init(global_data);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

    this->scheduleUpdate();

    // creating a keyboard event listener
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

bool HelloWorld::post_init(GlobalData global_data)
{
  this->global_data = global_data;
  level_data = global_data.levels[global_data.current_level_idx];

  level_collection.setLevel(level_data.filename);
  this->segs_start = level_data.segments;
  this->segs_left = level_data.segments;
  this->tick_speed = 1.0f/level_data.speed;

  blocks_holder = cocos2d::Sprite::create();
  blocks_holder->setAnchorPoint(Vec2(0, 0));
  blocks_holder->setPosition(0, 0);
  addChild(blocks_holder, 1);

  block_sprites.resize(level_collection.num_width*level_collection.num_height, nullptr);

  for (int w = 0; w < level_collection.num_width; w++)
  {
      for (int h = 0; h < level_collection.num_height; h++)
      {
          const int block_idx = (w*level_collection.num_height) + h;
          const auto block_type = level_collection.positions[w][h];
          if (block_type == BlockType::WALL)
          {
              auto block = cocos2d::Sprite::create("wall.png");
              const int x = w*8;
              const int y = h*8;

              block->setAnchorPoint(Vec2(0,0));
              block->setPosition(x,y);
              blocks_holder->addChild(block, 1);
          }
          else if (block_type == BlockType::UNIT)
          {
              auto block = cocos2d::Sprite::create("head_n.png");
              const int x = w*8;
              const int y = h*8;

              block->setAnchorPoint(Vec2(0.5f,0.5f));
              block->setPosition(x + 4,y + 4);
              blocks_holder->addChild(block, 1);
              unit = block;
              unit_x = w;
              unit_y = h;

          }
          else if (block_type == BlockType::EXIT)
          {
              auto block = cocos2d::Sprite::create("exit.png");
              const int x = w*8;
              const int y = h*8;

              block->setAnchorPoint(Vec2(0,0));
              block->setPosition(x,y);
              blocks_holder->addChild(block, 1);
          }
          else if (block_type == BlockType::HEALTH)
          {
              auto block = cocos2d::Sprite::create("plus.png");
              const int x = w*8;
              const int y = h*8;

              block->setAnchorPoint(Vec2(0,0));
              block->setPosition(x,y);
              blocks_holder->addChild(block, 1);
              block_sprites[block_idx] = block;
          }
          else if (block_type == BlockType::MINUS)
          {
              auto block = cocos2d::Sprite::create("minus.png");
              const int x = w*8;
              const int y = h*8;

              block->setAnchorPoint(Vec2(0,0));
              block->setPosition(x,y);
              blocks_holder->addChild(block, 1);
              block_sprites[block_idx] = block;
          }
      }
  }

  dir_next = level_data.dir;
  dir = level_data.dir;
  const float rot = dir_to_rotation(dir);
  unit->setRotation(rot);

  return true;
}

// Implementation of the keyboard event callback function prototype
void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
  if (std::find(heldKeys.begin(), heldKeys.end(), keyCode) == heldKeys.end())
    heldKeys.push_back(keyCode);
}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
  heldKeys.erase(std::remove(heldKeys.begin(), heldKeys.end(), keyCode), heldKeys.end());
  releasedKeys.push_back(keyCode);
}

Dir inc_dir(Dir dir) { return static_cast<Dir>((dir + 1) % 4); }
Dir dec_dir(Dir dir) { return static_cast<Dir>((dir + 3) % 4); }

template<typename C, typename T>
bool contains(const C& c, const T& t)
{
  return std::find(c.begin(), c.end(), t) != c.end();
}

void add_tail_part
(
    cocos2d::Node* layer,
    const int w,
    const int h,
    const Dir dnext,
    const Dir dprev,
    Color3B tail_color,
    LevelCollection& level_collection,
    std::vector<cocos2d::Sprite*>& tail_sprites
)
{
  cocos2d::Sprite* block = nullptr;
  float rotation = 0.0f;
  std::string image_name = "";


  if (dprev == Dir::WEST)
  {
      switch (dnext)
      {
        case Dir::NORTH:
          rotation = 270.0f;
          image_name = "angle.png";
          break;
        case Dir::SOUTH:
          rotation = 0.0f;
          image_name = "angle.png";
          break;
        default:
          rotation = 90.0f;
          image_name = "tail.png";
          break;
      }
  }
  else if (dprev == Dir::NORTH)
  {
      switch (dnext)
      {
        case Dir::WEST:
          rotation = 90.0f;
          image_name = "angle.png";
          break;
        case Dir::EAST:
          rotation = 0.0f;
          image_name = "angle.png";
          break;
        default:
          rotation = 0.0f;
          image_name = "tail.png";
          break;
      }
  }
  else if (dprev == Dir::SOUTH)
  {
      switch (dnext)
      {
        case Dir::WEST:
          rotation = 180.0f;
          image_name = "angle.png";
          break;
        case Dir::EAST:
          rotation = 270.0f;
          image_name = "angle.png";
          break;
        default:
          rotation = 0.0f;
          image_name = "tail.png";
          break;
      }
  }
  else if (dprev == Dir::EAST)
  {
      switch (dnext)
      {
        case Dir::NORTH:
          rotation = 180.0f;
          image_name = "angle.png";
          break;
        case Dir::SOUTH:
          rotation = 90.0f;
          image_name = "angle.png";
          break;
        default:
          rotation = 90.0f;
          image_name = "tail.png";
          break;
      }
  }

  level_collection.positions[w][h] = BlockType::TAIL;
  block = cocos2d::Sprite::create(image_name.c_str());
  const int x = w*8 + 4;
  const int y = h*8 + 4;

  block->setRotation(rotation);
  block->setAnchorPoint(Vec2(0.5,0.5));
  block->setPosition(x,y);
  block->setColor(tail_color);
  layer->addChild(block, 1);
  tail_sprites.push_back(block);
}



void HelloWorld::update(float dt)
{
  if (replacing_scene)
    return;

  timer_last_value += dt;

  for (auto k : heldKeys)
  {
      if (!contains(heldKeysLast, k))
          pressedKeys.push_back(k);
  }

  if (contains(pressedKeys, EventKeyboard::KeyCode::KEY_ESCAPE))
  {
      Director::getInstance()->end();
      return;
  }

  if(!dir_changed)
  {
    for (auto it = heldKeys.begin(); it < heldKeys.end(); ++it)
    {
        if (*it == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
        {
            dir_next = dec_dir(dir);
            dir_changed = true;
            heldKeys.erase(it);
            break;
        }
        else if (*it == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
        {
            dir_next = inc_dir(dir);
            heldKeys.erase(it);
            dir_changed = true;
            break;
        }
    }
  }

  if (timer_last_value > tick_speed || dir_changed)
  {
      BlockType p = level_collection.positions[unit_x][unit_y];
      const bool crash = (p == BlockType::TAIL || p == BlockType::WALL);
      const bool seg_stop = (segs_left <= 0);
      const bool level_done = p == BlockType::EXIT;
      const bool on_health = p == BlockType::HEALTH;
      const bool on_minus = p == BlockType::MINUS;

      if (level_done)
      {
          this->replacing_scene = true;
          global_data.current_level_idx++;
          const bool is_last_level = global_data.current_level_idx == global_data.levels.size();

          Scene* next_level = is_last_level ? EndScene::createScene() : HelloWorld::createScene(global_data);
          Director::getInstance()->replaceScene(TransitionFade::create(0, next_level, Color3B(255,255,255)));
          return;
      }
      else if (crash || seg_stop)
      {
          this->replacing_scene = true;
          Scene* next_level = HelloWorld::createScene(global_data);
          Director::getInstance()->replaceScene(TransitionFade::create(0, next_level, Color3B(255,255,255)));
          return;
      }
      else if (on_health)
      {
          const int block_idx = (unit_x*level_collection.num_height) + unit_y;
          cocos2d::Sprite* s = block_sprites[block_idx];
          blocks_holder->removeChild(s, true);
          block_sprites[block_idx] = nullptr;
          level_collection.positions[unit_x][unit_y] = BlockType::EMPTY;
          segs_left += level_data.health_value;
      }
      else if (on_minus)
      {
          for (int w = 0; w < level_collection.num_width; w++)
          {
            for (int h = 0; h < level_collection.num_height; h++)
            {
              if (level_collection.positions[w][h] == BlockType::TAIL)
                level_collection.positions[w][h] = BlockType::EMPTY;
            }
          }

          for (int i = 0; i < tail_sprites.size(); ++i)
          {
              auto sprite = tail_sprites[i];
              blocks_holder->removeChild(sprite, true);
          }
          tail_sprites.clear();

          const int block_idx = (unit_x*level_collection.num_height) + unit_y;
          cocos2d::Sprite* s = block_sprites[block_idx];
          blocks_holder->removeChild(s, true);
          block_sprites[block_idx] = nullptr;
          level_collection.positions[unit_x][unit_y] = BlockType::EMPTY;
      }

      const int unit_x_prev = unit_x;
      const int unit_y_prev = unit_y;

      segs_left--;
      Color3B tail_color;

      if (segs_left < 1)
        tail_color = Color3B::WHITE;
      else if (segs_left < 0.33f*segs_start)
        tail_color = Color3B::RED;
      else if (segs_left < 0.66f*segs_start)
        tail_color = Color3B::YELLOW;
      else
        tail_color = Color3B::GREEN;

      add_tail_part(blocks_holder, unit_x_prev, unit_y_prev, dir_next, dir, tail_color, level_collection, tail_sprites);

      float rot = 0.0f;
      dir = dir_next;


      switch (dir)
      {
        case NORTH:
          unit_y++;
          rot = 0.0f;
          break;
        case SOUTH:
          unit_y--;
          rot = 180.0f;
          break;
        case EAST:
          unit_x++;
          rot = 90.0f;
          break;
        case WEST:
          unit_x--;
          rot = 270.0f;
          break;
      }

      timer_last_value = 0;
      unit->setRotation(rot);
      unit->setPosition(unit_x*8 + 4, unit_y*8 + 4);

      dir_changed = false;
  }


  heldKeysLast = heldKeys;
  releasedKeys.clear();
  pressedKeys.clear();
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

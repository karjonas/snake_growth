#ifndef LEVELCOLLECTION_H
#define LEVELCOLLECTION_H

#include "BlockType.h"
#include <map>
#include <vector>
#include <2d/CCTMXTiledMap.h>

using cocos2d::TMXTiledMap;

class LevelCollection
{
public:
  LevelCollection();

  void setLevel(const std::string level_path);

  const int num_width = 40;
  const int num_height = 25;
  std::vector<std::vector<BlockType>> positions;

private:
  std::map<uint32_t, BlockType> pixelToBlockMap;
  void readPixmapBoard(TMXTiledMap* tiled_map);
};

#endif // LEVELCOLLECTION_H

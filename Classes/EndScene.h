#ifndef ENDSCENE_H
#define ENDSCENE_H

#include "cocos2d.h"

class EndScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(EndScene);

    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    void update(float dt) override;

    ////////////////////////////////////////////////////////////////

    std::vector<cocos2d::EventKeyboard::KeyCode> heldKeys;
    std::vector<cocos2d::EventKeyboard::KeyCode> heldKeysLast;
    std::vector<cocos2d::EventKeyboard::KeyCode> pressedKeys;
    std::vector<cocos2d::EventKeyboard::KeyCode> releasedKeys;
};

#endif // ENDSCENE_H

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "LevelCollection.h"
#include "GlobalData.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene(GlobalData global_data);

    virtual bool init();
    bool post_init(GlobalData global_data);

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    void update(float dt) override;

    ////////////////////////////////////////////////////////////////

    std::vector<cocos2d::EventKeyboard::KeyCode> heldKeys;
    std::vector<cocos2d::EventKeyboard::KeyCode> heldKeysLast;
    std::vector<cocos2d::EventKeyboard::KeyCode> pressedKeys;
    std::vector<cocos2d::EventKeyboard::KeyCode> releasedKeys;

    LevelCollection level_collection;
    LevelData level_data;

    cocos2d::Sprite* unit = nullptr;
    cocos2d::Sprite* blocks_holder = nullptr;
    int unit_x = 0;
    int unit_y = 0;

    Dir dir = Dir::NORTH;
    Dir dir_next = Dir::NORTH;

    float timer_last_value = 0.0f;
    int segs_left = 100;
    int segs_start = 100;

    float tick_speed = 1.0f;

    GlobalData global_data;

    bool replacing_scene = false;
    bool dir_changed = false;

    std::vector<cocos2d::Sprite*> block_sprites;
    std::vector<cocos2d::Sprite*> tail_sprites;

};

#endif // __HELLOWORLD_SCENE_H__
